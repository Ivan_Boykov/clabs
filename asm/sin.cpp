#include <stdio.h>
#include <cmath>
//вычислить синус разложением в ряд тейлора с заданной точностью
double tailor_sin(double x, double eps){
	char* msg="%lf\n";
	__asm{
finit
fldpi
fadd st, st
fld QWORD PTR x //6
m1: fprem1 
fstsw ax
sahf
jp m1


fxch
fstp st(0)
fld QWORD PTR eps//5
fld1//4
fld1 //3
fld st(3)//2
fmul st(4), st(0)
fldz //1
fld st(1) //0
//x^2, eps, n, factor, pow x, res, last 
cycle:

fadd st(1), st(0)
fabs
fcomip st(0), st(5)
jb END

fxch st(1)
fchs 
fmul st(0), st(5)
fxch st(1)
//x^2 5, eps 4, n 3, factor 2, pow x 1, res
fld st(3)
fld1
faddp st(1), st(0)
fmul st(3), st(0)
fld1
faddp st(1), st(0)
fmul st(3), st(0)
fxch st(4)
fstp st(0)
fld st(1)
fdiv st(0), st(3)
jmp cycle
END:

	}
}

int main(){
	double eps, x;
	while(scanf("%lf %lf", &x, &eps) ==2){
		double s=sin(x);
		double t_s= tailor_sin(x, eps);
		printf("x= %lf\nsin= %lf\ntsin= %lf\n diff= %lf\n", x, s, t_s, s-t_s);
	}
}