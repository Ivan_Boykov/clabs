#pragma once
#include <cstddef>
#include <stdexcept>
using std::istream;
using std::ostream;
namespace task1{
class IntArray{
	private:
		int* m_data=nullptr;
		size_t m_size;
		size_t m_capacity;
	public:
		IntArray(size_t size=10 , int n=0, size_t reserve=0);
		IntArray(const IntArray& obj);
		IntArray(IntArray&& obj);
		~IntArray();
		int& operator[](size_t index);
		int operator[](size_t index) const ;
void resize(size_t new_size);
void reserve(size_t res);
		size_t size() const { return m_size;}
		size_t capacity() const { return m_capacity- m_size;}
		void pushBack(int e);
		int popBack();
		//operators
		IntArray& operator=(const IntArray& obj);
		IntArray& operator=(IntArray&& obj);
		bool operator==(const IntArray& arr);
		bool operator!=(const IntArray& arr){return !(*this == arr);}
		bool operator<(const IntArray& arr) const;
		bool operator<=(const IntArray& arr) const{ return !(*this > arr);}
		bool operator>(const IntArray& arr) const;
		bool operator>=(const IntArray& arr) const{ return !(*this < arr);}
		friend IntArray operator+(const IntArray& a1, const IntArray& a2);
		friend ostream& operator<<(ostream& s, const IntArray& arr);
		friend istream& operator>>(istream& s, IntArray& arr);
};

}