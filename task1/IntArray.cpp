#include "IntArray.h"
#include <iostream>
using std::ostream;
namespace task1{
IntArray::IntArray(size_t size, int n, size_t reserve){
	std::cout <<"called constructor\n";
m_size=size;
	m_capacity=size+reserve;
	m_data= new int[m_capacity];
	for( size_t i=0; i< m_size; i++){
		m_data[i]=n;
	}
}
//copy constructor
IntArray::IntArray(const IntArray& obj){
	std::cout <<"called copy constructor\n";
	m_size=obj.m_size;
	m_capacity=m_size;
	m_data=new int[m_size];
	for(size_t i=0; i<m_size; i++){
		m_data[i]=obj.m_data[i];
	}
}
IntArray::IntArray(IntArray&& obj){
	std::cout <<"called moveing constructor\n";
	m_data=obj.m_data;
	m_size=obj.m_size;
	m_capacity=obj.m_capacity;
	obj.m_data=nullptr;
	obj.m_size=0;
	obj.m_capacity=0;
}
IntArray::~IntArray(){
	if(m_data){
		delete[] m_data;
	}
}
int& IntArray::operator[](size_t index){
	if( index >= m_size){
		throw std::out_of_range("������ �� �������� �������");
	}
	return m_data[index];
}
//const []
int IntArray::operator[](size_t index) const {
	if( index >= m_size){
		throw std::out_of_range("������ �� �������� �������");
	}
	return m_data[index];
}
void IntArray::resize(size_t new_size){
	if(new_size <= m_capacity){
		for(size_t i=m_size; i< new_size; i++){
			m_data[i]=0;
		}
		m_size=new_size;
	}
	else{
		int* tmp= new int[new_size];
		size_t i=0;
		for(; i< m_size; i++){
			tmp[i]=m_data[i];
		}
		for(; i<new_size; i++){
			tmp[i]=0;
		}
		delete[] m_data;
		m_data=tmp;
	}
}
void IntArray::pushBack(int e){
	if(m_size== m_capacity){
		resize(m_size+1);
	}
	m_data[m_size]=e;
	m_size++;
}
int IntArray::popBack(){
	int res;
	if(m_size==0) throw std::out_of_range("���������� ����� ������� �� ������� �������");
	res=m_data[--m_size];
	return res;
}
void IntArray::reserve(size_t res){
	int* tmp=new int[m_size+res];
	for( size_t i=0; i< m_size; i++){
		tmp[i]=m_data[i];
	}
	delete[] m_data;
	m_data=tmp;
}
IntArray& IntArray::operator=(const IntArray& obj){
	std::cout << "copy initilization"<< std::endl;
	if( this== &obj) return *this;
	if(m_data) delete[] m_data;
	m_size=obj.m_size;
	m_capacity=m_size;
	m_data=new int[m_size];
	for(size_t i=0; i< m_size; i++){
		m_data[i]=obj.m_data[i];
	}
	return *this;
}
IntArray& IntArray::operator=(IntArray&& obj){
	std::cout << "move initilization"<< std::endl;
	if( this== &obj) return *this;
	if(m_data) delete[]m_data;
	m_size=obj.m_size;
	m_capacity=obj.m_capacity;
	m_data=obj.m_data;
	obj.m_data=nullptr;
	obj.m_size=0;
	obj.m_capacity=0;
	return *this;
}
bool IntArray::operator==(const IntArray& arr){
	if(m_size != arr.m_size) throw std::length_error("������� ������ ����� �������� ����������");
	bool result=true;
	for(size_t i=0; result && i<m_size; i++){
		result=m_data[i]==arr.m_data[i];
	}
	return result;
}
IntArray operator+(const IntArray& a1, const IntArray& a2){
	IntArray res(a1.size() + a2.size());
	size_t res_i=0;
	for(size_t i=0; i<a1.size(); i++){
	res[res_i]=a1[i];
	res_i++;
	}
for(size_t i=0; i<a2.size(); i++){
	res[res_i]=a2[i];
	res_i++;
	}
	return res;
}

ostream& operator<<(ostream& s, const IntArray& arr){
	s<< arr.m_size;
	for(size_t i=0; i< arr.m_size; i++){
		s<< ' ' << arr.m_data[i];
	}
	return s;
}
istream& operator>>(istream& s, IntArray& arr){
	size_t size;
	s>> size;
	if(s.fail()) return s;
	IntArray tmp(size);
	for(size_t i=0; i<size; i++){
		s>> tmp[i];
	}
	if(!s.fail()) arr=std::move(tmp);
	return s;
}
bool IntArray::operator<(const IntArray& arr) const{
	bool result=true;
	size_t count= m_size < arr.m_size ? m_size : arr.m_size;
	for(size_t i=0; result && i<count; i++){
		result=m_data[i] < arr.m_data[i];
	}
	return result;
}
bool IntArray::operator>(const IntArray& arr) const{
	bool result=true;
	size_t count= m_size < arr.m_size ? m_size : arr.m_size;
	for(size_t i=0; result && i<count; i++){
		result=m_data[i] > arr.m_data[i];
	}
	return result;
}

}//end namespace