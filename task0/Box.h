#pragma once
#include "pch.h"
using std::ostream;
using std::istream;
namespace task0{
class Box{
	private:
		int length, width, height;
		double weight;
		long value;
	public:
		Box(int length, int width, int height, double weight, int value);
		Box();
		//getters
		int get_length() const;
		int get_width() const;
		int get_height() const;
		double get_weight() const{return weight;}
		long get_value() const {return value;}
		//сеттеры. бросают invalid_argument, если переданное число меньше или равно 1.
		void set_length(int l);
		void set_width(int i);
		void set_height(int i);
		void set_weight(double i);
		void set_value(int i);
		// prints  {<width>, <height>, <length>, <weight>, <value>}
		friend ostream& operator<<(ostream& s, Box& b) ;
		friend istream& operator>>(istream& s, Box& b) ;;
};
}