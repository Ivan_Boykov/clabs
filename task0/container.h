#pragma once
#include "pch.h"
#include "Box.h"
using namespace std;
namespace task0{
class Container{
	private:
		vector<Box> v;
		int length, height, width;
		double max_weight, cur_weight;
	public:
	//Р±СЂРѕСЃР°РµС‚ invalid_argument, РµСЃР»Рё Р·РЅР°С‡РµРЅРёСЏ РїР°СЂР°РјРµС‚СЂРѕРІ РјРµРЅСЊС€Рµ РёР»Рё СЂР°РІРЅС‹ 0
	Container(int length, int height, int width, double max_weight);
	// РґРѕР±Р°РІР»СЏРµС‚ РєРѕСЂРѕР±РєСѓ РІ РєРѕРЅС‚РµР№РЅРµСЂ. РµСЃР»Рё РјР°РєСЃРёРјР°Р»СЊРЅС‹Р№ РІРµСЃ РїСЂРµРІС‹С€РµРЅ, Р±СЂРѕСЃР°РµС‚ max_weight_exception. РІРѕР·РІСЂР°С‰Р°РµС‚ РёРЅРґРµРєСЃ РґРѕР±Р°РІР»РµРЅРЅРѕР№ РєРѕСЂРѕР±РєРё
size_t add(Box& b);
//С‚Рѕ-Р¶Рµ СЃР°РјРѕРµ, РЅРѕ Р±СЂРѕСЃР°РµС‚ out_of_range, РµСЃР»Рё РїРѕР·РёС†РёСЏ РІС‹С…РѕРґРёС‚ Р·Р° РіСЂР°РЅРёС†С‹ РєРѕРЅС‚РµР№РЅРµСЂР°.
	size_t add(Box& b, size_t i);
	//РІРѕР·РІСЂР°С‰Р°РµС‚ СЃСЃС‹Р»РєСѓ РЅР° i-С‚СѓСЋ РєРѕСЂРѕР±РєСѓ. Р±СЂРѕСЃР°РµС‚ out_of_range РµСЃР»Рё i РІС‹С…РѕРґРёС‚ Р·Р° РіСЂР°РЅРёС†С‹ РєРѕРЅС‚РµР№РЅРµСЂР°.
	Box& get(size_t i);
	//СѓРґР°Р»СЏРµС‚ i-С‚СѓСЋ РєРѕСЂРѕР±РєСѓ. Р±СЂРѕСЃР°РµС‚ out_of_range РµСЃР»Рё i РІС‹С…РѕРґРёС‚ Р·Р° РіСЂР°РЅРёС†С‹ РєРѕРЅС‚РµР№РЅРµСЂР°.
	void remove(size_t i);
	//РІРѕР·РІСЂР°С‰Р°РµС‚ РєРѕР»РёС‡РµСЃС‚РІРѕ РєРѕСЂРѕР±РѕРє
	size_t size() const{ return v.size();}
	//РІРѕР·РІСЂР°С‰Р°РµС‚ СЃСѓРјРјР°СЂРЅСѓСЋ СЃС‚РѕРёРјРѕСЃС‚СЊ РєРѕСЂРѕР±РѕРє
	long  total_value() const;
	//РІРѕР·РІСЂР°С‰Р°РµС‚ СЃСѓРјРјР°СЂРЅС‹Р№ РІРµСЃ РєРѕСЂРѕР±РѕРє.
	double total_weight() const;
	//С‚Рѕ-Р¶Рµ СЃР°РјРѕРµ, С‡С‚Рѕ Рё get
	Box& operator[](size_t i) {return v.at(i);}
	Box operator[](size_t i) const {return v.at(i);}
	/*
	Р—Р°РіСЂСѓР¶Р°РµС‚ РїР°СЂР°РјРµС‚СЂС‹ РєРѕРЅС‚РµР№РЅРµСЂР° РёР· РїРѕС‚РѕРєР°. Р§РёС‚Р°РµС‚ РїРѕРґСЂСЏРґ С‚СЂРё С†РµР»С‹С… С‡РёСЃР»Р° (С€РёСЂРёРЅР°, РІС‹СЃРѕС‚Р°, РґР»РёРЅРЅР°) bРё РѕРґРЅРѕ РґСЂРѕР±РЅРѕРµ(РјР°РєСЃРёРјР°Р»СЊРЅС‹Р№ РІРµСЃ).
	РїРѕСЃР»Рµ СЃС‡РёС‚С‹РІР°РЅРёСЏ РєРѕРЅС‚РµР№РЅРµСЂ РѕС‡РёС‰Р°РµС‚СЃСЏ.
	*/
	friend istream&  operator>>(istream& s, Container& c);
	/*РїРµС‡Р°С‚Р°РµС‚ РєРѕРЅС‚РµР№РЅРµСЂ РІ ostream. Р¤РѕСЂРјР°С‚:
	*/
	friend ostream& operator<<(ostream& s, Container& c) ;
};
class max_weight_exception{
	private:
		const char* msg;
	public:
	max_weight_exception(const char* msg = "РџСЂРµРІС‹С€РµРЅ РјР°РєСЃРёРјР°Р»СЊРЅРѕ РґРѕРїСѓСЃС‚РёРјС‹Р№ РІРµСЃ РєРѕРЅС‚РµР№РЅРµСЂР°"): msg(msg){}
		const char* what() const{return msg;}
};
}