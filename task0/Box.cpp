#include "Box.h"
using namespace std;
namespace task0{
Box::Box(){
	width=0;
	height=0;
	length=0;
	weight=0;
	value=0;
}
Box::Box(int length, int width, int height, double weight, int value): length(length) , width(width), height(height), weight(weight), value(value){
	if( length <=0 || height <=0 || width <=0 || weight <0) throw new std::invalid_argument("Размер или вес не может быть отрицательным.");
}
//getters
int Box::get_length() const{return length;}
int Box::get_width() const{return  width;}
int Box::get_height() const{return height; }
//long Box::get_value() const{ return value;}
//setters
void Box::set_length(int i){
	if(i<=0) throw new invalid_argument("Значение не может быть отрицательным");
	length=i;
}
void Box::set_width(int i){
	if(i<=0) throw new invalid_argument("Значение не может быть отрицательным");
	width=i;
}
void Box::set_height(int i){
	if(i<=0) throw new invalid_argument("Значение не может быть отрицательным");
	height=i;
}
void Box::set_weight(double i){
	if(i<=0) throw new invalid_argument("Значение не может быть отрицательным");
	weight=i;
}
void Box::set_value(int i){
	if(i<=0) throw new invalid_argument("Значение не может быть отрицательным");
	value=i;
}
ostream& operator<<(ostream& s, Box& b) {
	s<< "{" << b.width << ", " << b.height<< ", " << b.length << ", " <<b.weight << ", " <<b.value <<"}";
	return s;

}
istream& operator>>(istream& s, Box& b) {
	s>> b.width >>b.height>>b.length >>b.weight >> b.value ;
	return s;
}
}