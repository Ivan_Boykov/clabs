#include "Container.h"
using namespace std;
namespace task0{
Container::Container(int length, int height, int width, double max_weight): length(length), height(height), width(width), max_weight(max_weight) {
	if(length<=0 || height<=0 || width<=0 || max_weight<=0) throw new invalid_argument("�������� ������� �� ����� ���� ��������������");
	cur_weight=0;
}

size_t Container::add(Box& b){
	if(b.get_weight() + cur_weight > max_weight) throw new max_weight_exception();
	v.push_back(b);
	cur_weight+=b.get_weight();
	return v.size()-1;
}
size_t Container::add(Box& b, size_t i){
	if(b.get_weight() + cur_weight > max_weight) throw new max_weight_exception();
	if(i>v.size()) throw new out_of_range("insert behind the end of vector");
	v.insert(v.begin() + i, b);
	cur_weight+=b.get_weight();
	return i;
}
Box& Container::get(size_t i){
	return v.at(i);
}
void Container::remove(size_t i){
	Box& b=v.at(i);
	cur_weight-=b.get_weight();
	v.erase(v.begin() + i);
}
long Container::total_value() const {
	long value=0;
	for(auto b:v){
		value+=b.get_value();
	}
	return value;
}
double Container::total_weight() const {
return cur_weight;
}
//stream io
ostream& operator<<(ostream& s, Container& c) {
	s<< c.width << " " << c.height << " " << c.length << endl;
	for(Box b:c.v){
		s<< b << endl;
	}
	s<< "count: " << c.size();
	s<< "total value: " << c.total_value() <<endl;
	s<< "total weight: " << c.total_weight() << endl;
	return s;
}
istream& operator>>(istream& s, Container& c){
	s>> c.width >> c.height >> c.length >>c.max_weight;
	c.v.clear();
	return s;
}
}