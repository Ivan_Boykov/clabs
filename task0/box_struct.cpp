#include "box_struct.h"
Box::Box(){
width	=0;
height=0;
length=0;
weight=0;
value=0;
}
Box::Box(int l, int w, int h, double we, int val){
	length=l;
	width=w;
	height=h;
	weight=we;
	value=val;
}
//stream io
ostream& operator<<(ostream& s, Box& b){
	s << "{" << b.width << ", " << b.height << ", " << b.length << ", " << b.weight << ", " << b.value << "}";
	return s;
}
void box_to_stream(ostream& s, Box& b){
	s << "�����: " << b.width <<"�" <<b.height <<"�" << b.length << " ��";
	s << " ���: " << b.weight << "��.  ���������: " << (b.value/100.d) << " ���.";
}
istream& operator>>(istream& s, Box& b){
	char c;
	s>> c;
	if(c!='{') throw runtime_error("������ ��� ������ �� ������");
	s >> b.width;
	s >> c;
	s >> b.height;
	s >> c;
	s >> b.length;
	s >> c;
	s >> b.weight;
	s >> c;
	s >> b.value;
	s >> c;
	if(s.fail() || c != '}') throw runtime_error("������ ��� ������ �� ������");
	return s;
}
//equals
bool operator==(Box& b1, Box& b2){
	return b1.length == b2.length && b1. height == b2.height && b1.width == b2.width && b1.weight == b2.weight && b1.value== b2.value;
}
// functions
long all_cost( vector<Box>& v){
	long cost=0;
	for(auto b:v){
		cost+=b.value;
	}
	return cost;
}
bool all_size_less_then(vector<Box>& v, int sum){
	long size=0;
	for(auto b:v){
		size+=b.width;
		size+=b.height;
		size+=b.length;
		if(size > sum) return false;
		size=0;
	}
	return true;
}
double max_weight_less_size(vector<Box>& v, int maxV){
	double max_weight=-1;
	for(auto b:v){
		int volume=b.width * b.length * b.height;
		if(volume <= maxV && b.weight > max_weight){
			max_weight=b.weight;
		}
	}
	return max_weight;
}
size_t min_box_index(vector<Box>& v){
	if(v.size() ==0) return -1;
	Box min = v.front();
	size_t min_i =0;
	for(size_t i=1; i< v.size(); i++){
		if(v.at(i).length < min.length && v.at(i).height < min.height && v.at(i).length < min.length){
			min_i =i;
			min=v.at(i);
		}
	}
	return min_i;
}
bool can_insert(vector<Box> v){
	if(v.size() ==0) return false;
	size_t min_i=min_box_index(v);
	Box min=v.at(min_i);
	v.erase(v.begin() +min_i);
	while(v.size() > 0){
		size_t tmp_i=min_box_index(v);
		Box tmp_b= v.at(tmp_i);
		if(tmp_b.length > min.length && tmp_b.width > min.width && tmp_b.height > min.height){
			min=tmp_b;
			min_i=tmp_i;
			v.erase(v.begin()+ tmp_i);
		} else return false;
	}
return true;	
}