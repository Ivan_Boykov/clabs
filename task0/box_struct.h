#pragma once
#include "pch.h"
using namespace std;
struct Box {
	int length, width, height;
	double weight;
	long value;
	Box(int l, int w, int h, double we, int val);
	Box();
};
istream& operator>>(istream& s, Box& b);
ostream& operator<<(ostream& s, Box& b);
void box_to_stream(ostream& s, Box& b);
bool operator==(Box& b1, Box& b2);
long all_cost( vector<Box>&);
bool all_size_less_then(vector<Box>&, int sum);
double max_weight_less_size(vector<Box>&, int maxV);
bool can_insert(vector<Box>);